import { Component, OnInit } from '@angular/core';

import { PassdataService } from '../../services/passdata.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {

  sentence: string;
  count: string;
  value = 10;
  conditional;
  constructor(
    public passdataServices: PassdataService
  ) { }

  ngOnInit() {
    this.countdown();
  }

  countdown() {
    this.passdataServices.getPass().subscribe(
      h => this.conditional = h
    );

    console.log(this.conditional);
    if ( this.conditional) {
      setTimeout(() => {
        if (this.value > 0) {
          this.value -= 1;
          this.countdown();
        }
      }, 1000);
      this.count = this.value.toString();
    }
  }

verificar() {
  if (this.text()) {
    alert('Esto es palíndromo');
  } else {
    alert('Esto no es palíndromo');
  }
}

  text() {
    let palabra = (this.sentence).toLowerCase();
    palabra = palabra.replace(/ /g, '');

    for (let i = 0; i < palabra.length; i++) {
      if (palabra[i] !== palabra[palabra.length - i - 1]) {
        return false;
    }
  }

    return true;
  }

}
