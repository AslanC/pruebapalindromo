import { Component, OnInit } from '@angular/core';

import { PassdataService } from './../../services/passdata.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  nameplayer;
  visible;
  constructor(
    public passdataService: PassdataService
  ) { }

  ngOnInit() {
  }

  dessapear() {
    this.visible = 'none';
    this.passdataService.passname();
  }

}
