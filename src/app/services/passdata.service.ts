import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PassdataService {

  pass: boolean;

  constructor() { }

  passname() {
    this.pass = true;
  }

  getPass(): Observable<boolean> {
    return of(this.pass);
  }
}
